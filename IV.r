
setwd("C:/Users/lucas/Desktop/Python/sscpraktikum")
rm(list=ls())
library(httpgd)
source("minorticks.r")
#read files crystal
AuAusmall <- read.table(file = "data/ToF_IV_Lifetime/IV_measure_Au&Au_-20 - 20V", header = TRUE,  sep = '	')
V <- AuAusmall$Voltage..V.
A <- AuAusmall$Current..A.
plot(V,A,
xlab = "Voltage [V]",
ylab = "Current [A]")
fitV <- V[8:40]
fitA <- A[8:40]
Rmodel <- lm(fitA~fitV)
print(summary(Rmodel))
abline(Rmodel, col = "red")
dev.copy2pdf(file = "data/Tof_IV_Lifetime/IVAuAu-20to20V.pdf", height=10, width=15)
#p calculation
coeff <- Rmodel$coefficients
R <- coeff[2]^(-1)
L <- 2.11 # thickness crystal in mm for contact length
a <- 4 * pi #area of contat in mm^2
#print(R)
p <-(R*a)/L
print(p)
#sd p calcuation
prozentsd <- (1.153e-07)/(1.980e-06)
sdR <- R*prozentsd
sdp <- (sdR*a)/L
print(sdp)


#read files moltenfilm
thinfilm <- read.table(file = "data/ToF_IV_Lifetime/IV_measure_thinfilm_-0.5 - 0.7V_p.3", header = TRUE,  sep = '	')
V <- thinfilm$Voltage..V.
A <- thinfilm$Current..A.
plot(V,A,
xlab = "Voltage [V]",
ylab = "Current [A]")
fitV <- V[74:84]
fitA <- A[74:84]
Rmodel <- lm(fitA~fitV)
print(summary(Rmodel))
abline(Rmodel, col = "red")
dev.copy2pdf(file = "data/Tof_IV_Lifetime/IVthinfilm-0.5to0.7Vp3.pdf", height=10, width=15)
#p calculation
coeff <- Rmodel$coefficients
R <- coeff[2]^(-1)
L <-  0.005# thickness crystal in mm for contact length
a <- 100 #area of contat in mm^2 10x10 mm grid
#print(R)
p <-(R*a)/L
print(p)
#sd p calcuation
prozentsd <- (6.004e-05)/(1.881e-03)
sdR <- R*prozentsd
sdp <- (sdR*a)/L
print(sdp)