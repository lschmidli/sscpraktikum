
import matplotlib.pylab as plt
import pandas as pd

# Load the additional data file
file_path = 'C:/Users/lucas/Desktop/Python/sscpraktikum/data/Molten_film/2024-03-21_14-56-36_templog.dat'
data = pd.read_csv(file_path, delim_whitespace=True, header=None, names=["Index", "Temperature"])

# Plotting the additional temperature data
fig, ax = plt.subplots(figsize=(15, 10))

# Plot the temperature vs index
ax.plot(data["Index"] / 60, data["Temperature"], label='Temperature Profile')

# Customize the plot
ax.set_xlabel('Time [min]')
ax.set_ylabel('Temperature (°C)')
# ax.set_title('Temperature Profile from Data File')
# ax.legend()

# Save the plot to a file
output_file_path = 'molfilmtprofminutes.png'
plt.savefig(output_file_path)

# Show the plot
plt.show()
