setwd("C:/Users/lucas/Desktop/Python/sscpraktikum")
rm(list=ls())
library(httpgd)
library(data.table)
library(magicaxis)
library(ggplot2)
source("minorticks.r")
#functions
rff0 <- function(file) {
    data <- fread(file, header = TRUE, sep = ",",skip = 20)
    start <- which(data[[1]]>=0)[1]
    if (!is.na(start)) {
    filtered_data <- data[start:.N]
  } else {
    stop("No rows with first column >= 0 found.")
  }
  return(filtered_data)
}
#get data
file1 <- "C:/Users/lucas/Desktop/Python/sscpraktikum/data/ToF_IV_Lifetime/Lifetime_highsamplerate_1.csv"
file2 <- "C:/Users/lucas/Desktop/Python/sscpraktikum/data/ToF_IV_Lifetime/Lifetime_lowsamplerate_1.csv"
file3 <- "C:/Users/lucas/Desktop/Python/sscpraktikum/data/ToF_IV_Lifetime/Lifetime_lowsamplerate_1_200Hz.csv"
file4 <- "C:/Users/lucas/Desktop/Python/sscpraktikum/data/ToF_IV_Lifetime/Lifetime_lowsamplerate_2_200Hz.csv"
file5 <- "C:/Users/lucas/Desktop/Python/sscpraktikum/data/ToF_IV_Lifetime/Lifetime_lowsamplerate_1_500Hz.csv"
file6 <- "C:/Users/lucas/Desktop/Python/sscpraktikum/data/ToF_IV_Lifetime/Lifetime_lowsamplerate_2_500Hz.csv"
lths1 <- rff0(file1)
ltls1 <- rff0(file2)
ltls200hz1 <- rff0(file3)
ltls200hz2 <- rff0(file4)
ltls500hz1 <- rff0(file5)
ltls500hz2 <- rff0(file6)
ltls500hz2 <- ltls500hz2[ltls500hz2[[2]] >= 0, ] #filter data to y > 0 because of false data evaluation else

# ##plots and calc method 1
# fittedt <- seq(0,0.0009,0.0000001)
# #plot 1 + calc
# plot(ltls200hz1$TIME,ltls200hz1$CH1, xaxt = 'n', ylab = "Signal [V]", xlab = "Time [s]")
# x1 <- c(0, 2e-4, 4e-4, 6e-4, 8e-4, 1e-3)
# x_labels <- c(0, expression(2~"*"~10^-4), expression(4~"*"~10^-4), expression(6~"*"~10^-4), expression(8~"*"~10^-4), expression(1~"*"~10^-3))
# axis(1, at = x1, labels = x_labels)
# baseline <- data.frame(seq(0,0.01,0.00001), rep(min(ltls200hz1$CH1),1001))
# matlines(baseline[1],baseline[2], col = "blue", lty = 2)
# fitdata <-ltls200hz1[1:10]
# fity <- fitdata[[2]]
# fitt <- seq(0,0.000009,0.000001)
# fit <- lm(fity~fitt)
# print(summary(fit))
# abline(fit, col = "blue")
# tau1 <- 2.79e-5
# points(tau1,0.0007917188, col = "green")
# dev.copy2pdf(file = "plots/Lifetime/Lifetime200hz1method1.pdf", height=10, width=15)

# #plot 2 + calc
# ltls200hz2 <- ltls200hz2[-2] # remove outlier will be shown later as red point points(2e-7, 0.0034, col = "red", pch = 1)
# plot(ltls200hz2$TIME,ltls200hz2$CH1, xaxt = 'n', ylab = "Signal [V]", xlab = "Time [s]")
# x2 <- c(0, 2e-5, 4e-5, 6e-5, 8e-5, 1e-4)
# x_labels2 <- c(0, expression(2~"*"~10^-5), expression(4~"*"~10^-5), expression(6~"*"~10^-5), expression(8~"*"~10^-5), expression(1~"*"~10^-4))
# axis(1, at = x2, labels = x_labels2)
# baseline2 <- data.frame(seq(0,0.01,0.00001), rep(min(ltls200hz2$CH1),1001))
# matlines(baseline2[1],baseline2[2], col = "blue", lty = 2)
# fitdata2 <-ltls200hz2[1:10]
# fitt2 <- fitdata2[[1]]
# fity2 <- fitdata2[[2]]
# fit2 <- lm(fity2~fitt2)
# print(summary(fit2))
# fittedy2 <- predict(fit2, list(fitt2 = fittedt))
# abline(fit2, col = "blue")
# print(fittedt[69])
# print(fittedt[70])
# tau2 <- 4.02e-6
# points(tau2, 0.002489844, col = "green")
# points(2e-7, 0.0034, col = "red", pch = 2) # unused outlier
# dev.copy2pdf(file = "plots/Lifetime/Lifetime200hz2method1.pdf", height=10, width=15)
# #plot 3 + calc
# ltls500hz1 <- ltls500hz1[-2] # remove outlier
# plot(ltls500hz1$TIME,ltls500hz1$CH1,xaxt="n", ylab = "Signal [V]", xlab = "Time [s]")
# x3 <- c(0, 1e-5, 2e-5, 3e-5, 4e-5, 5e-5,6e-5,7e-5)
# x_labels3 <- c(0, expression(1~"*"~10^-5), expression(2~"*"~10^-5), expression(3~"*"~10^-5), expression(4~"*"~10^-5), expression(5~"*"~10^-5),expression(6~"*"~10^-5),expression(7~"*"~10^-5))
# axis(1, at = x3, labels = x_labels3)
# baseline3 <- data.frame(seq(0,0.01,0.00001), rep(0.0028,1001))
# matlines(baseline3[1],baseline3[2], col = "blue", lty = 2)
# fitdata3 <-ltls500hz1[1:8]
# points(2e-7,0.0038, col = "red", pch = 2) # unused outlier
# fity3 <- fitdata3[[2]]
# fitt3 <- fitdata3[[1]]
# fit3 <- lm(fity3~fitt3)
# print(summary(fit3))
# abline(fit3, col = "blue")
# tau3 <- 2.64e-6
# points(tau3, 0.0028, col = "green")
# dev.copy2pdf(file = "plots/Lifetime/Lifetime500hz1method1.pdf", height=10, width=15)

# #plot 4 + calc
# plot(ltls500hz2$TIME,ltls500hz2$CH1, xaxt="n", ylab = "Signal [V]", xlab = "Time [s]")
# x4 <- c(0, 1e-4, 2e-4, 3e-4, 4e-4, 5e-4)
# x_labels4 <- c(0, expression(1~"*"~10^-4), expression(2~"*"~10^-4), expression(3~"*"~10^-4), expression(4~"*"~10^-4), expression(5~"*"~10^-4))
# axis(1, at = x4, labels = x_labels4)
# baseline4 <- data.frame(seq(0,0.01,0.00001), rep(0.0011,1001))
# matlines(baseline4[1],baseline4[2], col = "blue", lty = 2)
# fitdata4 <-ltls500hz2[1:8]
# fity4 <- fitdata4[[2]]
# fitt4 <- fitdata4[[1]]
# fit4 <- lm(fity4~fitt4)
# print(summary(fit4))
# abline(fit4, col = "blue")
# tau4 <- 1.99e-5
# points(tau4, 0.0011, col = "green")
# dev.copy2pdf(file = "plots/Lifetime/Lifetime500hz2method1.pdf", height=10, width=15)


#plots calc method 2
#plot1
time <- ltls200hz1$TIME * 1e6
signal <-  (ltls200hz1$CH1 - min(ltls200hz1$CH1)) * 1000

idx <- which(time <= 5)

plot(time , signal,
  xaxt = 'n',
  ylab = "Signal [mV]", 
  xlab = expression("Time"~"["*mu*s*"]"),
  log = "y",
  axes = FALSE
)
box()

points(time[idx], signal[idx],
  col = "red",
  pch = 16
)

model <- lm(log10(signal[idx]) ~ time[idx])
abline(model, col = "blue")


magaxis(1,hersh = TRUE)
magaxis(2, hersh = TRUE, logpretty = TRUE)
#summary
t1 <- -1 / coef(model)[2]
dev.copy2pdf(file = "plots/Lifetime/Lifetime200hz1method2.pdf", height=10, width=15)

# plot 2
time <- ltls200hz2$TIME * 1e6
signal <-  (ltls200hz2$CH1 - min(ltls200hz2$CH1)) * 1000

idx <- which(time <= 1)

plot(time , signal,
  xaxt = 'n',
  ylab = "Signal [mV]", 
  xlab = expression("Time"~"["*mu*s*"]"),
  log = "y",
  axes = FALSE
)
box()

points(time[idx], signal[idx],
  col = "red",
  pch = 16
)

model <- lm(log10(signal[idx]) ~ time[idx])
abline(model, col = "blue")


magaxis(1,hersh = TRUE)
magaxis(2, hersh = TRUE, logpretty = TRUE)
#summary
t2 <- -1 / coef(model)[2]
dev.copy2pdf(file = "plots/Lifetime/Lifetime200hz2method2.pdf", height=10, width=15)
#plot 3

time <- ltls500hz1$TIME * 1e6
signal <-  (ltls500hz1$CH1 - min(ltls500hz1$CH1)) * 1000

idx <- which(time <= 2)

plot(time , signal,
  xaxt = 'n',
  ylab = "Signal [mV]", 
  xlab = expression("Time"~"["*mu*s*"]"),
  log = "y",
  axes = FALSE
)
box()

points(time[idx], signal[idx],
  col = "red",
  pch = 16
)

model <- lm(log10(signal[idx]) ~ time[idx])
abline(model, col = "blue")


magaxis(1,hersh = TRUE)
magaxis(2, hersh = TRUE, logpretty = TRUE)
#summary
t3 <- -1 / coef(model)[2]
dev.copy2pdf(file = "plots/Lifetime/Lifetime500hz1method2.pdf", height=10, width=15)

# plot 4
time <- ltls500hz2$TIME * 1e6
signal <-  (ltls500hz2$CH1 - min(ltls500hz2$CH1)) * 1000

idx <- which(time <= 5)

plot(time , signal,
  xaxt = 'n',
  ylab = "Signal [mV]", 
  xlab = expression("Time"~"["*mu*s*"]"),
  log = "y",
  axes = FALSE
)
box()

points(time[idx], signal[idx],
  col = "red",
  pch = 16
)

model4 <- lm(log10(signal[idx]) ~ time[idx])
abline(model4, col = "blue")


magaxis(1,hersh = TRUE)
magaxis(2, hersh = TRUE, logpretty = TRUE)
#summary
t4 <- -1 / coef(model4)[2]
dev.copy2pdf(file = "plots/Lifetime/Lifetime500hz2method2.pdf", height=10, width=15)
print(t1)
print(t2)
print(t3)
print(t4)