setwd("C:/Users/lucas/Desktop/Python/sscpraktikum")
rm(list=ls())
library(httpgd)
library(zoom)
library(magicaxis)
source("minorticks.r")


# DATA PARSING ###################################
data_location <- "data/ToF_IV_Lifetime/ToF/"
files <- list.files(path = "data/ToF_IV_Lifetime/ToF/", include.dirs = TRUE)
files <- paste0(data_location, files)

data <- lapply(files, read.csv)


lmIntx <- function(fit1, fit2, rnd=2) { # Function for intercept of two linear models
  b1<- fit1$coefficient[1]  #y-int for fit1
  m1<- fit1$coefficient[2]  #slope for fit1
  b2<- fit2$coefficient[1]  #y-int for fit2
  m2<- fit2$coefficient[2]  #slope for fit2
  if(m1==m2 & b1==b2) {print("Lines are identical")
  } else if(m1==m2 & b1 != b2) {print("Lines are parallel")
  } else {
    x <- (b2-b1)/(m1-m2)      #solved general equation for x
    y <- m1*x + b1            #plug in the result
    data.frame(x=round(x, rnd), y=round(y, rnd))
  }
}

for(i in 1:length(data)){
    df <- data[[i]]
    time <- df[,2] * 1e6 - 10 # microseconds
    voltage <- df[,3] * 1e3 # millivolts

    par(mfrow = c(1,2))
    plot(time, voltage,
        xlab = expression("Time"~"["*mu*s*"]"),
        ylab = "Voltage [mV]",
        axes = FALSE,
        type = "l"
    )
    box()
    magaxis(hersh = TRUE)
    
    plot(time, voltage,
        xlab = expression("Time"~"["*mu*s*"]"),
        ylab = "Voltage [mV]",
        axes = FALSE,
        log = "xy",
        type = "l"
    )
    box()
    magaxis(hersh = TRUE)

    dev.copy2pdf(file = sprintf("plots/ToF/ToF_%s.pdf", i), width = 8, height = 6)
}

#FITS ###################################################

 par(mfrow = c(1,1)) # again only one plot shown for the ividuell plots
 # tof 13
    df <- data[[13]]
    time <- df[,2] * 1e6  - 10 #  microseconds without shift
    voltage <- df[,3] * 1e3 # millivolts

    plot(time, voltage,
        xlab = expression("Time"~"["*mu*s*"]"),
        ylab = "Voltage [mV]",
        axes = FALSE,
        log = "xy",
        type = "l",
        xlim = c(0.03,10)
    )
    box()
    magaxis(hersh = TRUE)
    # manual region border
    abline(v = 0.1, col = "blue", lwd = 2, lty = 2)
    abline(v = 0.8, col = "blue", lwd = 2, lty = 2)
    abline(v = 1.5, col = "green", lwd = 2, lty = 2)
    abline(v = 7, col = "green", lwd = 2, lty = 2)
# fits

#Plateau
p_time <- time[time >= 0.1 & time <= 0.8] # get x values, microseconds
p_x <- (p_time + 10) / 1e6 # for geting y values based on x
p_voltage <- df$CH1[df$Time..s %in% p_x] * 1e3 # get y values
p_time <- p_time[1:length(p_voltage)] # workaround for adjusting length of x to length of ye3
    p_fit <- lm(log10(p_voltage) ~ log10(p_time)) # dont ask questions why the fuck this works and normal v~t dont
    
    # fit curve
    abline(p_fit, col = "red")
    #print(summary(p_fit))

#Fall
f_time <- time[time >= 2 & time <= 7] # get x values
f_x <- (f_time + 10) / 1e6 # for geting y values based on x
f_voltage <- df$CH1[df$Time..s %in% f_x] * 1e3 # get y values
f_time <- f_time[1:length(f_voltage)] # workaround for adjusting length of x to length of ye3
    f_fit <- lm(log10(f_voltage) ~ log10(f_time)) # dont ask questions why the fuck this works and normal v~t dont

    # fit curve
    abline(f_fit, col = "red")
    # print(summary(f_fit))
    intercept <- lmIntx(p_fit, f_fit)
    print(intercept) # values are log10!!!!!
     abline(v = 1.34896, col = "#c8ff00", lwd = 2, lty = 2)
legend("bottomleft",
        legend = c("Plateau region", "Fall region", "Transient time", "Fit curves"),
        col = c("blue", "green", "#c8ff00", "red"),
        lty = c(2, 2, 2, 1)
        )

dev.copy2pdf(file = sprintf("plots/ToF/ToF_13_transient_time.pdf"), width = 15, height = 10)

# tof 12
    df <- data[[12]]
    time <- df[,2] * 1e6  - 10 #  microseconds without shift
    voltage <- df[,3] * 1e3 # millivolts

    plot(time, voltage,
        xlab = expression("Time"~"["*mu*s*"]"),
        ylab = "Voltage [mV]",
        axes = FALSE,
        log = "xy",
        type = "l"
        , xlim = c(0.03,10)
    )
    box()
    magaxis(hersh = TRUE)
    # manual region border
    abline(v = 0.08, col = "blue", lwd = 2, lty = 2)
    abline(v = 1.1, col = "blue", lwd = 2, lty = 2)
    abline(v = 1.5, col = "green", lwd = 2, lty = 2)
    abline(v = 10, col = "green", lwd = 2, lty = 2)
# fits
#Plateau
p_time <- time[time >= 0.08 & time <= 1.1] # get x values, microseconds
p_x <- (p_time + 10) / 1e6 # for geting y values based on x
p_voltage <- df$CH1[df$Time..s %in% p_x] * 1e3 # get y values
p_time <- p_time[1:length(p_voltage)] # workaround for adjusting length of x to length of ye3
    p_fit <- lm(log10(p_voltage) ~ log10(p_time))
    # fit curve
    abline(p_fit, col = "red")
    # print(summary(p_fit))

#Fall
f_time <- time[time >= 1.5 & time <= 7] # get x values
f_x <- (f_time + 10) / 1e6 # for geting y values based on x
f_voltage <- df$CH1[df$Time..s %in% f_x] * 1e3 # get y values
f_time <- f_time[1:length(f_voltage)] # workaround for adjusting length of x to length of ye3
    f_fit <- lm(log10(f_voltage) ~ log10(f_time)) # dont ask questions why the fuck this works and normal v~t dont

    # fit curve
    abline(f_fit, col = "red")
    # print(summary(f_fit))

    intercept <- lmIntx(p_fit, f_fit)
    print(intercept) # values are log10!!!!!
    abline(v = 1.318256739, col = "#c8ff00", lwd = 2, lty = 2)
legend("bottomleft",
        legend = c("Plateau region", "Fall region", "Transient time", "Fit curves"),
        col = c("blue", "green", "#c8ff00", "red"),
        lty = c(2, 2, 2, 1)
        )

dev.copy2pdf(file = sprintf("plots/ToF/ToF_12_transient_time.pdf"), width = 15, height = 10)

# tof 10
    df <- data[[10]]
    time <- df[,2] * 1e6  - 10 #  microseconds without shift
    voltage <- df[,3] * 1e3 # millivolts

    plot(time, voltage,
        xlab = expression("Time"~"["*mu*s*"]"),
        ylab = "Voltage [mV]",
        axes = FALSE,
        log = "xy",
        type = "l"
        ,ylim = c(1,6)
    )
    box()
    magaxis(hersh = TRUE)
    # manual region border
    abline(v = 0.03, col = "blue", lwd = 2, lty = 2)
    abline(v = 1.1, col = "blue", lwd = 2, lty = 2)
    abline(v = 2, col = "green", lwd = 2, lty = 2)
    abline(v = 10, col = "green", lwd = 2, lty = 2)
# fits
#Plateau
p_time <- time[time >= 0.03 & time <= 1.1] # get x values, microseconds
p_x <- (p_time + 10) / 1e6 # for geting y values based on x
p_voltage <- df$CH1[df$Time..s %in% p_x] * 1e3 # get y values
p_time <- p_time[1:length(p_voltage)] # workaround for adjusting length of x to length of ye3
    p_fit <- lm(log10(p_voltage) ~ log10(p_time))
    # fit curve
    abline(p_fit, col = "red")
    # print(summary(p_fit))

#Fall
f_time <- time[time >= 2 & time <= 7] # get x values
f_x <- (f_time + 10) / 1e6 # for geting y values based on x
f_voltage <- df$CH1[df$Time..s %in% f_x] * 1e3 # get y values
f_time <- f_time[1:length(f_voltage)] # workaround for adjusting length of x to length of ye3
    f_fit <- lm(log10(f_voltage) ~ log10(f_time)) # dont ask questions why the fuck this works and normal v~t dont

    # fit curve
    abline(f_fit, col = "red")
    # print(summary(f_fit))

    intercept <- lmIntx(p_fit, f_fit)
    print(intercept) # values are log10!!!!!
    abline(v = 1.905460718, col = "#c8ff00", lwd = 2, lty = 2)
legend("bottomleft",
        legend = c("Plateau region", "Fall region", "Transient time", "Fit curves"),
        col = c("blue", "green", "#c8ff00", "red"),
        lty = c(2, 2, 2, 1)
        )

dev.copy2pdf(file = sprintf("plots/ToF/ToF_10_transient_time.pdf"), width = 15, height = 10)

# tof 11
    df <- data[[11]]
    time <- df[,2] * 1e6  - 10 #  microseconds without shift
    voltage <- df[,3] * 1e3 # millivolts

    plot(time, voltage,
        xlab = expression("Time"~"["*mu*s*"]"),
        ylab = "Voltage [mV]",
        axes = FALSE,
        log = "xy",
        type = "l"
        ,ylim = c(1,6)
    )
    box()
    magaxis(hersh = TRUE)
    # manual region border
    abline(v = 0.04, col = "blue", lwd = 2, lty = 2)
    abline(v = 1.1, col = "blue", lwd = 2, lty = 2)
    abline(v = 2, col = "green", lwd = 2, lty = 2)
    abline(v = 10, col = "green", lwd = 2, lty = 2)
# fits
#Plateau
p_time <- time[time >= 0.04 & time <= 1.1] # get x values, microseconds
p_x <- (p_time + 10) / 1e6 # for geting y values based on x
p_voltage <- df$CH1[df$Time..s %in% p_x] * 1e3 # get y values
p_time <- p_time[1:length(p_voltage)] # workaround for adjusting length of x to length of ye3
    p_fit <- lm(log10(p_voltage) ~ log10(p_time))
    # fit curve
    abline(p_fit, col = "red")
    # print(summary(p_fit))

#Fall
f_time <- time[time >= 2 & time <= 7] # get x values
f_x <- (f_time + 10) / 1e6 # for geting y values based on x
f_voltage <- df$CH1[df$Time..s %in% f_x] * 1e3 # get y values
f_time <- f_time[1:length(f_voltage)] # workaround for adjusting length of x to length of ye3
    f_fit <- lm(log10(f_voltage) ~ log10(f_time)) # dont ask questions why the fuck this works and normal v~t dont

    # fit curve
    abline(f_fit, col = "red")
    # print(summary(f_fit))

    intercept <- lmIntx(p_fit, f_fit)
    print(intercept) # values are log10!!!!!
    abline(v = 1.621810097, col = "#c8ff00", lwd = 2, lty = 2)
legend("bottomleft",
        legend = c("Plateau region", "Fall region", "Transient time", "Fit curves"),
        col = c("blue", "green", "#c8ff00", "red"),
        lty = c(2, 2, 2, 1)
        )

dev.copy2pdf(file = sprintf("plots/ToF/ToF_11_transient_time.pdf"), width = 15, height = 10)

#Mobility Plot and Calculation ###########################################################
U <- c(10, 11, 12, 13) # bias voltage in V
L <- 0.211 #cm
t_r <- c(1.905460718, 1.621810097, 1.318256739, 1.34896) / 1e6 #us convertion into s

mu <- (L*L) / (t_r*U)

plot(U, mu,
    xlab = "Bias Voltage [V]",
    ylab =  expression("Mobility "~mu~" ["*cm^2*V^{-1}*s^{-1}*"]")
    )

dev.copy2pdf(file = sprintf("plots/ToF/ToF_mobility_vs_biasV.pdf"), width = 15, height = 10)