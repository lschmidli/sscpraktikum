#!/usr/bin/env python
################################################################################
#
# The script is based on the plotxy script by Balint Aradi.
# Modified by Dmitry A. Ryndyk.
#
################################################################################
#
#  Wrapper around matplotlib options to plot simple xy-graphs
#
################################################################################
#
#  Copyright (c) 2016, Balint Aradi
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  1. Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  2. Redistributions in binary form must reproduce the above
#  copyright notice, this list of conditions and the following
#  disclaimer in the documentation and/or other materials provided
#  with the distribution.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
#  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
#  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#  OF THE POSSIBILITY OF SUCH DAMAGE.
#
################################################################################
from __future__ import print_function
import argparse
import numpy as np
import matplotlib.pylab as plt
import math
import sys
import matplotlib

parser = argparse.ArgumentParser(description='Plot 2D data as ((N)X)Y plot')
parser.add_argument('--version', action='version', version='1.1')
parser.add_argument('-L', '--no-legend', action='store_false', default=True,
                    dest='legend', help='omit legends')
parser.add_argument('-G', '--no-grid', action='store_false', default='True',
                    dest='grid', help='omit grid')
parser.add_argument('datafile', nargs='+',
                    help='file containing the data')
parser.add_argument('-C', '--no-colors', action='store_false', default=True,
                    dest='colors', help='omit coloring')
#parser.add_argument('-s', '--skip-rows', type=int, dest='skiprows',
#                    metavar='ROWS', default=0,
#                    help='nr. of rows to skip (default: 0)')
#parser.add_argument('-x', '--x-column', dest='xcol', type=int, default=1,
#                    help='column of the x values (default: 1)')
parser.add_argument('-y', '--y-columns', dest='ycols', metavar='YCOL',
                    type=int, nargs='+', default=None,
                    help='list of the columns for the y values'
                    ' (default: all columns)')
parser.add_argument('-l', '--labels', dest='labels', metavar='LABEL',
                    nargs='+', default=None,
                    help='label for each data column (default: filename:'
                    'column_number)')
parser.add_argument('--xlabel', dest='xlabel', default='', 
                    help='label for x-axis')
parser.add_argument('--ylabel', dest='ylabel', default='', 
                    help='label for y-axis')
parser.add_argument('--title', dest='title', default='',
                    help='title of the plot')
parser.add_argument('--xlimits', dest='xlim', nargs=2, type=float, 
                    default=None, help='limits for x axis.')
parser.add_argument('--ylimits', dest='ylim', nargs=2, type=float, 
                    default=None, help='limits for y axis.')
parser.add_argument('--xshift', type=float, default=0.0,
                    help='shift all x values by this')
parser.add_argument('--yshift', type=float, default=0.0,
                    help='shift all y values by this')
parser.add_argument('--logscale', action='store',
                    choices=['none', 'x', 'y', 'xy'], default='none',
                    dest='logscale', help='use log scaling on selected axis '\
                    '(default: none)')
parser.add_argument('--zero-line', action='store_true', default=False,
                    dest='zeroline', help='draw extra line for y=0')

args = parser.parse_args()
plt.figure(num=1, figsize=(8.27,5.83), facecolor='w', edgecolor='k')
axis = plt.gca()

if args.logscale == 'none':
    plot = plt.plot
elif args.logscale == 'x':
    plot = plt.semilogx
elif args.logscale == 'y':
    plot = plt.semilogy
else:
    plot = plt.loglog

if not args.colors:
    axis.set_color_cycle([ (0.0, 0.0, 0.0) ])
k = -1    
for datafile in args.datafile:
    k = k+1
    data = np.loadtxt(datafile, skiprows=0)
    yy = data[:,3:]
    colshift = 2
    yy = yy + args.yshift
    if args.ycols is None:
        ycols = np.arange(yy.shape[1])
    else:
        ycols = np.array(args.ycols) - colshift
    xx = np.arange(1, len(data) + 1)    
    for ii in range(yy.shape[1]):
        if ii not in ycols:
            continue
        k = k + ii
        if args.labels is not None and len(args.labels) > k:
            label = args.labels[k]
        else:
            label = '{}:{:d}'.format(datafile, colshift + ii)
        plot(xx, yy[:,ii], label=label)

if args.zeroline:
    plot([ min(xx), max(xx) ], [ 0.0, 0.0 ], 'k--')
if args.legend:
    plt.legend()
if args.grid:
    plt.grid()
if args.xlabel:
    plt.xlabel(args.xlabel,{'fontsize':20})
if args.ylabel:
    plt.ylabel(args.ylabel,{'fontsize':20})
if args.title:
    plt.title(args.title)
if args.xlim:
    plt.xlim(args.xlim[0], args.xlim[1])
if args.ylim:
    plt.ylim(args.ylim[0], args.ylim[1])

# generate list of k-points following some high-symmetry line in
k_label=[r"$K$", r"$\Gamma$", r"$M$", r"$K$"]  
axis.set_xticks([1.,41.,81.,121.])
axis.set_xticklabels(k_label) 
plt.figure(num=1).tight_layout()

plt.xticks(size = 15)
plt.yticks(size = 15)
plt.gcf().subplots_adjust(bottom=0.15)
plt.gcf().subplots_adjust(left=0.15)

plt.show()
