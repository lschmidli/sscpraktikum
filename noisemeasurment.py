import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#index in s (1 index sind 500 us), then s in dose (5 uA -> ~50 uGy/s)
#open file
filepath = "C:/Users/lucas/Desktop/Python/sscpraktikum/data/noisestuff.dat"
# df = pd.read_csv(filepath, sep = ",")
data = pd.read_csv(filepath, sep = ',')

noise = data['noise']
noise2 = data['noise2']

plt.scatter(noise,noise2)
plt.xlabel('Dose [uGy]')
plt.ylabel('Noise ^2')
plt.grid(True)
plt.show()