rm(list=ls())


# LIBRARIES ################################

library(httpgd)
library(magicaxis)
library(chemCal)
source("minorticks.r")
source("fig_label.r")

# ABSORPTION SPECTROSCOPY DATA ################################

datafilm <- read.csv(file = "data/Absorption/Film_1.csv", skip = 1, sep = ";")
dataSC <- read.csv(file = "data/Absorption/singlecrystal_1.csv", skip = 1, sep = ";")

# CONSTANTS #############################

h <- 4.135667696e-15 #planck constant in eV * s
c <- 299792458 #speed of light in m/s


# HARVESTING FILM DATA ###################

wl <- datafilm[,1]
abs <- datafilm[,2]

en <- h*c / (wl*1e-9) #calculating the energy corresponding to every wavelength in eV
d <- 5e-4 #thickness in centimeters (5 um)
alpha <- 2.303 * abs / d


#FILM PLOTS LAYOUT SETTINGS ###############################

par(mfrow = c(3,2))


# Wavelength vs absorbance plot #######################

if (FALSE){ # Used to plot or not
plot(wl, abs,
    type = "l",
    xlab = "Wavelength [nm]",
    ylab = "Absorbance",
) #plot absorbance
}


# FILM ENERGY VS ABSORBANCE PLOT ##################################
if(FALSE){
plot(en, abs, 
    xlab = "",
    ylab = "Absorbance",
    type = "l",
    xaxt = "n",
    yaxt = "n"
)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)
}

# FILM ENERGY VS ABSORBANCE COEFFICIENT PLOT ########################

plot(en, alpha, type = "l",
    xlab = "",
    ylab = "",
    xaxt = "n",
    yaxt = "n"
)
fig_label("a)", cex =2)

mtext(expression(alpha * "[cm" ^-1 * "]"), 2, line = 2)
mtext("Energy [eV]", 1, line = 2)
magaxis(2, hersh = TRUE, majorn = 5, minorn = 10, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 5, cex.axis = 1.1)

# dev.copy2pdf(file = "plots/Absorption/film_abs_coef_vs_en.pdf", width = 8, height = 6)

#FILM TAUC PLOT ##############################

y <- (alpha * h * (c / (wl * 1e-9)))^2 #calculate y of tauc plot with r = 2

plot(en, y,
    xlab = "",
    ylab = '',
    type = "l",
    xaxt = "n",
    yaxt = "n"
)

fig_label("b)", cex =2)
mtext("Energy [eV]", 1, line = 2)
mtext(expression((alpha * h * nu)^2), side = 2, line = 2)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)

idx <- which(en >= 2.33 & en <= 2.37)

points(en[idx], y[idx],
    col = "red",
    pch = 16
)

model <- lm(y[idx] ~ en[idx])
abline(model, col = "blue")
gap <- inverse.predict(model, 0)$Prediction # predict intercept of x axis


text(gap + .35, y = 5e8,
    bquote(E[g] * "=" * " " * .(round(gap, 2)) * " " * eV),
    cex = 1.2
)

# legend("topright",
#     legend = c("Fitted points", "Linear fit"),
#     pch = c(16, NA),
#     lty = c(NA, 1),
#     col = c("red", "blue")
# )

# dev.copy2pdf(file = "plots/Absorption/film_tauc.pdf", width = 8, height = 6)
## FILM URBACH ENERGY ########################

plot(en, log(abs),
    type = "l",
    xaxt = "n",
    yaxt = "n",
    xlab = "",
    xlim = c(2.2, 2.5),
    # ylim = c(6.5, 10),
    ylab = ""
)
fig_label("c)", cex =2)

mtext("Energy [eV]", 1, line = 2)
mtext(expression(ln~(A)), 2, line = 2)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2)
magaxis(2, hersh = TRUE, minorn = 5)

idx <- which(en >= 2.31 & en <= 2.355)

points(en[idx], log(abs[idx]),
    col = "red",
    pch = 16
)



urbach_model <- lm(log(abs[idx]) ~ en[idx])
slope <- coef(urbach_model)[2]
urbach_energy <- 1 / slope

# legend("topright",
#     legend = c("Fitted points", "Linear fit"),
#     pch = c(16, NA),
#     lty = c(NA, 1),
#     col = c("red", "blue")
# )

abline(urbach_model, col = "blue")
text(2.4, -.5, bquote(E[U]~"="~.(round(urbach_energy*1000, 1))~"meV"), cex = 1.2)
# dev.copy2pdf(file = "plots/Absorption/film_urbach.pdf", width = 8, height = 6)


# SINGLE CRYSTAL DATA HARVESTING #####################

wl <- dataSC[,1]
abs <- dataSC[,2]

en <- h*c / (wl*1e-9) #calculating the energy corresponding to every wavelength in eV
d <- 0.25 #thickness of crystal in centimeters (2.5 mm)
alpha <- 2.303 * abs / d # Absorption coefficient in cm^-1

# CRYSTAL ENERGY VS ABSORBANCE PLOT ##################################

if(FALSE){

plot(en, abs, 
    xlab = "",
    ylab = "Absorbance",
    type = "l",
    xaxt = "n",
    yaxt = "n"
)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)
}

# CRYSTAL ENERGY VS ABSORBANCE COEFFICIENT PLOT ########################

plot(en, alpha, type = "l",
    xlab = "",
    ylab = "",
    xaxt = "n",
    yaxt = "n"
)
fig_label("d)", cex =2)

mtext("Energy [eV]", 1, line = 2)
magaxis(2, hersh = TRUE, minorn = 10, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)
mtext(expression(alpha * "[cm" ^-1 * "]"), 2, line = 2)
# dev.copy2pdf(file = "plots/Absorption/sc_abs_coef_vs_en.pdf", width = 8, height = 6)
#CRYSTAL TAUC PLOT ##############################

y <- (alpha * h * (c / (wl * 1e-9)))^2 #calculate y of tauc plot with r = 1/2

plot(en, y,
    xlab = "",
    ylab = '',
    type = "l",
    xaxt = "n",
    yaxt = "n",
    xlim = c(2.2,2.5),
    ylim = c(0, 2e4)
)
fig_label("e)", cex =2)

mtext("Energy [eV]", 1, line = 2)
mtext(expression((alpha * h * nu)^2), side = 2, line = 2)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)

idx <- which(en >= 2.255 & en <= 2.27)

points(en[idx], y[idx],
    col = "red",
    pch = 16
)

model <- lm(y[idx] ~ en[idx])
abline(model, col = "blue")
gap <- inverse.predict(model, 0)$Prediction # predict intercept of x axis

text(gap + .09, y = 2500,
    bquote(E[g] * "=" * " " * .(round(gap, 2)) * " " * eV),
    cex = 1.2
)

# legend("topright",
#     legend = c("Fitted points", "Linear fit"),
#     pch = c(16, NA),
#     lty = c(NA, 1),
#     col = c("red", "blue")
# )

# dev.copy2pdf(file = "plots/Absorption/sc_tauc.pdf", width = 8, height = 10)

## CRYSTAL URBACH ENERGY

plot(en, log(abs),
    type = "l",
    xaxt = "n",
    yaxt = "n",
    xlab = "",
    ylab = "",
    xlim = c(2, 2.5),
    # ylim = c(6.5, 10),
)
fig_label("f)", cex =2)

idx <- which(en >= 2.23 & en <= 2.267)

mtext("Energy [eV]", 1, line = 2)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2)
magaxis(2, hersh = TRUE, minorn = 5)
points(en[idx], log(abs[idx]),
    col = "red",
    pch = 16
)

urbach_model <- lm(log(abs[idx]) ~ en[idx])
slope <- coef(urbach_model)[2]
urbach_energy <- 1 / slope

abline(urbach_model, col = "blue")
text(2.35, 0, bquote(E[U]~"="~.(round(urbach_energy*1000, 1))~"meV"), cex = 1.2)
mtext(expression(ln~(A)), 2, line = 2)

# legend("topright",
#     legend = c("Fitted points", "Linear fit"),
#     pch = c(16, NA),
#     lty = c(NA, 1),
#     col = c("red", "blue")
# )



#  dev.copy2pdf(file = "plots/Absorption/all_plots.pdf", width = 8, height = 10)