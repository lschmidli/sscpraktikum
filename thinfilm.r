rm(list = ls()) #nolint
par(new = FALSE, mar = c(5, 4, 4, 2) + 0.1, mfrow = c(1,1))
# LIBRARIES ################################

library(httpgd)
library(magicaxis)
library(chemCal)
library(colorspace)
source("minorticks.r")

# CONTROLS FOR PLOTTING ############################
plotJV <- TRUE
plotPCE <- TRUE
plotEQE <- FALSE
# CONSTANTS #############################

h <- 6.62607015e-34 #planck constant in J * s
c <- 299792458 #speed of light in m/s
e_charge <- 1.60217663e-19 # in coulomb
pixel_area <- 0.16 #in cm^2

total_light_power <- .001 * pixel_area # .001 W/cm^2 * 0.16 cm^2 gives the total power delivered by LED in J
wl <- 626e-9 # wl in meters
photon_energy <- h * c / wl # energy of one photon

# DATA PARSING ##################################

# function to read data from multiple files
read.tables <- function(file.names, ...) {
    require(plyr)
    ldply(file.names, function(fn) data.frame(Filename=fn, read.table(fn, skip = 1, ...)))
}

data_location <- "data/ThinFilms/IV/"
plot_location <- "plots/ThinFilms/"
filenames_dark <- c() # constructing the dark file name vector
for (device in 1:5) {
   for (pixel in 1:6) {
        filename <- sprintf("s%s_",device)
        filename <- paste(data_location, filename, sprintf("%sRDARK.dat", pixel), sep = "")
        filenames_dark <- c(filenames_dark, filename)
   }
}

filenames_light <- c() #constructing the light file name vector
for (device in 1:5) {
   for (pixel in 1:6) {
        filename <- sprintf("s%s_",toString(device))
        filename <- paste(data_location, filename, sprintf("%sRLIGHT.dat", toString(pixel)), sep = "")
        filenames_light <- c(filenames_light, filename)
   }
}

data_dark <- read.tables(filenames_dark) # extracting the data
data_light <- read.tables(filenames_light) # extracting the data

rm(filenames_dark) # cleanup
rm(filenames_light)

idx_file_change <- c() # to store locations where new plot starts
i <- 2
while (i <= length(data_dark[,1])) { # storing them by looking at file name in matrix
   filename <- data_dark[i, 1]
   filename_previous <- data_dark[i - 1, 1]
   if(filename != filename_previous){
      idx_file_change <- c(idx_file_change, i) #store location where it changes
   }
   i <- i + 1
}

idx_file_change <- c(1, idx_file_change, length(data_dark[,1])) #adding 0 and last number

# PLOTTING JV CURVES ########################################################
good_pixels <- c(1, 9, 19, 20, 23, 24, 25, 28, 29, 30) # hand-picked

VOC_vec <- c() # vectors for storing VOC, JSC, and PCE for all pixels
JSC_vec <- c()
EQE_vec <- c()

for (i in good_pixels) { # plotting all pixels by using indexes in idx_file_change
   range <- idx_file_change[i]:(idx_file_change[i + 1] - 1)

   filename <- data_dark[range[2], 1] # filename shenanigans
   filename <- gsub(data_location, plot_location, filename) 
   filename <- gsub(".dat", ".pdf", filename)

   
   voltage <- data_light[range, 2]
   current_dark <- data_dark[range, 3]
   current_light <- data_light[range, 3]
   
   current_density_dark <- current_dark / pixel_area * 1000 #milliAmpere
   current_density_light <- current_light / pixel_area * 1000

   JSC_idx <- which.min(abs(voltage))
   VOC_idx <- which.min(abs(current_density_light))

   JSC <- abs(current_density_light[JSC_idx])
   VOC <- abs(voltage[VOC_idx])

   JSC_vec <- c(JSC_vec, JSC) #append to empty vectors for storage of data
   VOC_vec <- c(VOC_vec, VOC)
   
   photocurrent <- current_light - current_dark # calculate photocurrent
   power_output <- abs(photocurrent) * abs(voltage) # calculate power output

   EQE <- (abs(photocurrent[JSC_idx]) / e_charge) / ((total_light_power / photon_energy)) * 100
   EQE_vec <- c(EQE_vec, EQE)
   PCE <- power_output / total_light_power * 100


   
   if(plotJV){ # to switch plotting on or off

   plot(-voltage, -current_density_dark,
      type = "l",
      xlab = "Voltage [V]",
      ylab = "",
      ylim = range(-current_density_dark, -current_density_light * 1.1),
      col = "#8f1919",
      xaxt = "n",
      yaxt = "n",
      main = filename
   )
   
   lines(-voltage, -current_density_light,
      col = "#a89911"
   )


   


   mtext(expression("Current Density"~"["*mA/cm^2*"]"),2, line = 2)
   mtext(sprintf("EQE = %s", EQE))
   magaxis(2, hersh = TRUE, cex.axis = 1, grid = TRUE, axis.cex = 0.7)
   magaxis(1, hersh = TRUE, cex.axis = 1, grid = TRUE)

   legend("topleft",
      legend = rev(c("Dark", "Light")),
      lty = c(1,1),
      col = rev(c("#8f1919", "#a89911"))
   )

   # dev.copy2pdf(file = filename, width = 8, height = 6) # save all plots as pdfs
   }

   if(plotPCE){
      plot(voltage, PCE,
      type = "l",
      xlab = "Voltage [V]",
      ylab = "Power Conversion Efficiency %"
      )
   }

}


# PLOTTING EQE FOR BEST PIXELS ######################################################

if(plotEQE){
for (i in good_pixels) { # cycle over correct pixels
   range <- idx_file_change[i]:(idx_file_change[i + 1]) # same as before
   
   filename <- data_dark[range[2], 1] # filename shenanigans
   filename <- gsub(data_location, plot_location, filename) 
   filename <- gsub(".dat", ".pdf", filename)

   voltage <- data_light[range, 2] # picking data
   current_dark <- data_dark[range, 3]
   current_light <- data_light[range, 3]
   photocurrent <- current_light - current_dark

   EQE <- (abs(photocurrent) / e_charge) / (total_light_power / photon_energy)
   EQE <- EQE * 100

   plot(voltage, EQE,
      type = "l",
      xaxt = "n",
      yaxt = "n",
      xlab = "Voltage [V]",
      ylab = "External Quantum Efficiency %",
      main = filename,
      ylim = c(0,120)
   )
   magaxis(2, hersh = TRUE, cex.axis = 1, grid = TRUE, axis.cex = 0.7)
   magaxis(1, hersh = TRUE, cex.axis = 1, grid = TRUE)
}
}

layout_matrix <- matrix(c(1,1,2,2),nrow = 1, byrow = TRUE)
layout(layout_matrix)

# VOC, JSC AND PCE SCATTER PLOT ###########################################################

# plot VOC
plot(1:length(good_pixels), VOC_vec,
   axes = FALSE, pch = 16, xlab = "", ylab = "", ylim = c(0.75, 0.95)
)

magaxis(1, hersh = TRUE, majorn = 5, minorn = 2)
mtext("Pixel number", 1, line = 3)

magaxis(2, hersh = TRUE,)
mtext(expression(V[oc]~"["*V*"]"), 2, line = 2.5)

box()

# plot JSC
plot(1:length(good_pixels), JSC_vec, axes = FALSE, pch = 15, col = "red", xlab = "", ylab = "")
box()
magaxis(1, minorn = 2, hersh = TRUE)
magaxis(2, hersh = TRUE)
mtext(expression(J[sc]~"["*mA/cm^2*"]"), 2, line = 2.5)
mtext("Pixel number", 1, line = 2.5)