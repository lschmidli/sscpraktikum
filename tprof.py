# imnport stuff
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sb


# Define the data for the plot
time = [0, 2, 4, 7, 8.5, 11.5, 16.5, 18.5]
temperature = [25, 25, 400, 400, 550, 550, 25, 25]


# Plot the temperature profile
plt.plot(time, temperature, color='orange', linewidth=2)

# Add annotations
plt.text(1.5, 225, '200 °K/h', fontsize=12, ha='center')
plt.text(1, 0, 'RT', fontsize=12, ha='center')
plt.text(17.5, 0, 'RT', fontsize=12, ha='center')

plt.text(5.5, 335, '400 °C\n3h', fontsize=12, ha='center')
plt.text(10, 485, '550 °C\n3h', fontsize=12, ha='center')
plt.text(6.25, 475, '100 °K/h', fontsize=12, ha='center')

plt.axis('off')

# Show the plot
plt.grid(False)
plt.savefig('plots/temperature_profile_plot.png', bbox_inches='tight', dpi=600)
plt.show()