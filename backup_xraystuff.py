# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


file_path = "data/xray_detection/AuAu/Photocurrent_vs_time_-5V_3_X_ray_pulses_50kV_79uA.dat"
data = pd.read_csv(file_path, sep = "\t")

time = data['Time (s)']
current = -data['Current (A)']


plt.plot(time, current)
plt.xlabel('Time (s)')
plt.ylabel('Current (A)')
plt.grid(True)
plt.show()

# %%
first_n = 25
last_n = 7
time_fit = np.concatenate([time[5:first_n], time[94:100]]) # fitting intervals
current_fit = np.concatenate([current[5:first_n], current[94:100]])

# def exp_decay(x, a, b, c, d, e, f):
#    return a*x**5 + b*x**4 + c*x**3 + d*x**2 + e*x + f
def exp_decay(x, a, b, c):
    return a * np.exp(-b*x) + c

params_exp, _ = curve_fit(exp_decay, time_fit, current_fit)

time_linspace = np.linspace(0, np.max(time), num = np.size(time)) # fit until when? 

fit = exp_decay(time_linspace, *params_exp)
curren = current - fit
 
plt.plot(time_linspace, fit)
plt.scatter(time_fit, current_fit)
plt.show()

# %%
plt.plot(time, current)
plt.scatter(time, fit, color = 'red')
plt.show()

# %%
curry = curren[20:]
plt.scatter(time[20:], curry)
plt.show()

# %%
photocurrent = np.mean(curry[(time > 30) & (curry > 2e-7) ]) - np.mean(curry[(time > 30) & (curry < 2e-7)])
# play with the time
photocurrent

# with open("shit.txt", "a")


